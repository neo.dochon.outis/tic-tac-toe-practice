import React,{useState} from 'react';
import './App.css';
import Squre from './Person/squre'
import Result from './Person/result'



function App (){

  const [state,setState]=useState({

    player:'cross',
    homes:[
      'empty','empty','empty',
      'empty','empty','empty',
      'empty','empty','empty'
    ]
  })

  

   function changeHome(homeNumber){

    const positions = [...state.homes];
    positions[homeNumber] = state.player;


    setState({
      player : state.player === 'cross'?'circle':'cross',
      homes:positions
    })
  }    


  function introduceWinner(homes){

    if(homes[0]==='circle' && homes[1]==='circle' && homes[2] === 'circle') return 'circle'
    if(homes[3]==='circle' && homes[4]==='circle' && homes[5] === 'circle') return 'circle'
    if(homes[6]==='circle' && homes[7]==='circle' && homes[8] === 'circle') return 'circle'

    if(homes[0]==='circle' && homes[3]==='circle' && homes[6] === 'circle') return 'circle'
    if(homes[1]==='circle' && homes[4]==='circle' && homes[7] === 'circle') return 'circle'
    if(homes[2]==='circle' && homes[5]==='circle' && homes[8] === 'circle') return 'circle'


    if(homes[0]==='circle' && homes[4]==='circle' && homes[8] === 'circle') return 'circle'
    if(homes[2]==='circle' && homes[4]==='circle' && homes[6] === 'circle') return 'circle'




    if(homes[0]=== 'cross' && homes[1]=== 'cross' && homes[2] === 'cross') return 'cross'
    if(homes[3]==='cross' && homes[4]==='cross' && homes[5] === 'cross') return 'cross'
    if(homes[6]==='cross' && homes[7]==='cross' && homes[8] === 'cross') return 'cross'

    if(homes[0]==='cross' && homes[3]==='cross' && homes[6] === 'cross') return 'cross'
    if(homes[1]==='cross' && homes[4]==='cross' && homes[7] === 'cross') return 'cross'
    if(homes[2]==='cross' && homes[5]==='cross' && homes[8] === 'cross') return 'cross'


    if(homes[0]==='cross' && homes[4]==='cross' && homes[8] === 'cross') return 'cross'
    if(homes[2]==='cross' && homes[4]==='cross' && homes[6] === 'cross') return 'cross'

  
    if(homes.every(home => home !=='empty')) return 'game end without winner'
  
  
  }

 function reset (){
   setState({
    player:'cross',
    homes:[
      'empty','empty','empty',
      'empty','empty','empty',
      'empty','empty','empty'
    ]
   })
 }
  const winners = introduceWinner(state.homes)

  
return (<div className='App'>


    <div className='row'>
      
    <Squre className='squres ' home={0} value={state.homes[0]} changeHome={changeHome} />
    <Squre className='squres ' home={1} value={state.homes[1]} changeHome={changeHome}/>
    <Squre className='squres ' home={2} value={state.homes[2]} changeHome={changeHome}/>
    <Squre className='squres ' home={3} value={state.homes[3]} changeHome={changeHome}/>
    <Squre className='squres ' home={4} value={state.homes[4]} changeHome={changeHome}/>
    <Squre className='squres ' home={5} value={state.homes[5]} changeHome={changeHome}/>
    <Squre className='squres ' home={6} value={state.homes[6]} changeHome={changeHome}/>
    <Squre className='squres ' home={7} value={state.homes[7]} changeHome={changeHome}/>
    <Squre className='squres ' home={8} value={state.homes[8]} changeHome={changeHome}/>
   
    </div>

    {winners && <Result winner={winners} clear={reset}/>}
    </div>
    )

  



}
export default App;
