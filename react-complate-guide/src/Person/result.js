import  React from 'react';


function Result (props){



    return (

      <div>
          {props.winner === 'circle' && 'circle won the game'}
          {props.winner === 'cross' && 'cross won the game'}
          {props.winner === 'game end without winner' && 'game tie!'}

           <button onClick={props.clear}>clear</button>

      </div>
    )

}


export default Result